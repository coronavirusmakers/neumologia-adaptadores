# Neumologia Adaptadores

Adaptadores para aparatos de neumología realizados en impresión 3D

Materiales de impresión recomendandos: PA12 > PETG (incoloro) > PLA (incoloro)


## Adaptador 22-15 Pared Doble
![](./img/Adaptador%2022-15%20Pared%20Doble.png)

## Adaptador 22-15 Pared Doble + V. Antiretorno
![](.img/Adaptador%20%2022-15%20Pared%20doble%20+%20Valvula%20Antiretorno%20A.png)
![](./img/Adaptador%20%2022-15%20Pared%20doble%20+%20Valvula%20Antiretorno%20B.png)

## Adaptador de Tubo Flexible (por exterior)
![](./img/Adaptador%20Flexible%2022-22%20v1.png)

## Adaptador de Tubo Rígido (por interior)
![](./img/Conector%20Rigido%2022-15.png)
![](./img/Conector%20Rigido%2022-22.png)

## Adaptador inhalador a tubo respirador 22mm V1.0
![](./img/Adaptador%20inhalador%20a%20tubo%20respirador%2022mm%20V1.0%20A.png)
![](./img/Adaptador%20inhalador%20a%20tubo%20respirador%2022mm%20V1.0%20B.png)
![](.img/Adaptador%20inhalador%20a%20tubo%20respirador%2022mm%20V1.0%20C.png)

## Conexión codo 58º de tubo 22mm
![](./img/Conexion%20Codo%2058%C2%BA%20tubo%2022%20mm.png)

## T Connector 22 to 19 internal diameter
![](./img/Conector%20Rigido%20T%2022-19-19.png)

## T Connector 22mm to 22mm
![](./img/Conector%20Rigidio%20T%2022-22.png)

## Tapones Tubo 22mm 19mm y 15mm
![](./img/Tapones%20Tubo.png)
